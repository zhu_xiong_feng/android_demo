package com.study.demo.app;

import android.webkit.WebView;

public interface OnReceivedErrorListener {
    void onReceivedError(WebView view, int errorCode, String description, String failingUrl);
}
