package com.study.demo.app;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;


import com.study.demo.R;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * create by AaronYang on 2018/6/12.
 * email: 1390939057@qq.com
 * github: AaronYang23
 * describe: 服务协议
 */
public class ProtocolActivity extends BaseActivity {

    @BindView(R.id.status_bar)
    TextView mTvStatusBar;
    @BindView(R.id.back)
    FrameLayout back;
    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.wv_protol)
    DWebView webView;

    @Override
    public int getLayoutId() {
        return R.layout.ac_protol_term;
    }

    @Override
    public void initView() {
        title.setText("服务协议");

    }

    @Override
    public void initData() {
        /*String str = readAssetsTxt("用户服务协议");
        tvProtocol.setText(str);*/

       /* webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");*/
        webView.loadUrl("file:///android_asset/用户服务协议.html");

    }

    @OnClick({R.id.back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }


    /**
     * 读取assets下的txt文件，返回utf-8 String
     *
     * @param fileName 不包括后缀
     * @return
     */
    public String readAssetsTxt(String fileName) {
        try {
            //v2editclose an AssetManager instance for your application's package
            InputStream is = getAssets().open(fileName + ".txt");
            int size = is.available();
            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            // Convert the buffer into a string.
            String text = new String(buffer, "utf-8");
            // Finally stick the string into the text view.
            return text;
        } catch (IOException e) {
            // Should never happen!
//            throw new RuntimeException(e);
            e.printStackTrace();
        }
        return "读取错误，请检查文件名";
    }

    //销毁Webview
    @Override
    protected void onDestroy() {
        if (webView != null) {
            webView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            webView.clearHistory();

            ((ViewGroup) webView.getParent()).removeView(webView);
            webView.destroy();
            webView = null;
        }
        super.onDestroy();
    }
}
