package com.study.demo.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * create by AaronYang on 2018/5/24.
 * email: 1390939057@qq.com
 * github: AaronYang23
 * describe: Activity基类
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Unbinder unbinder;
    protected View rootView; //根布局


//    private LoadingDialog mLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        rootView = LayoutInflater.from(this).inflate(getLayoutId(), null, false);
        unbinder = ButterKnife.bind(this);

        initView();
        initData();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();

    }


    //绑定服务成功
    protected void bindServered() {

    }

    public abstract int getLayoutId();

    public abstract void initView();

    public abstract void initData();


    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
        System.gc();//

    }


}
