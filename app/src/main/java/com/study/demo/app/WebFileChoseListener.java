package com.study.demo.app;

import android.webkit.ValueCallback;

public interface WebFileChoseListener {

    void getFile(ValueCallback valueCallback);

}
