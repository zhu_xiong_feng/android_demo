package com.study.demo.app;

import android.net.Uri;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * created by Administrator
 * on 2020/6/18
 */

public class MyWebChromeClient extends WebChromeClient {

    private WebFileChoseListener webFileChoseListener;
    // 3.0 + 调用这个方法
    public void openFileChooser(ValueCallback filePathCallback, String acceptType) {
        if (webFileChoseListener!= null){
            webFileChoseListener.getFile(filePathCallback);
        }
    }

    // Android > 4.1.1 调用这个方法
    public void openFileChooser(ValueCallback<Uri> filePathCallback, String acceptType, String capture) {
        if (webFileChoseListener!= null){
            webFileChoseListener.getFile(filePathCallback);
        }
    }

    // Android > 5.0 调用这个方法
    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        if (webFileChoseListener!= null){
            webFileChoseListener.getFile(filePathCallback);
        }
        return true;
    }

    public void setBnWebFileChoseListener(WebFileChoseListener webFileChoseListener) {
        this.webFileChoseListener= webFileChoseListener;
    }

}

