package com.study.demo;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.study.demo.app.PrivateDialog;
import com.study.demo.app.ProtocolActivity;
import com.study.demo.app.TermActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PrivateDialog.getInstace().message("").sure("同意").cancle("退出应用")
                .setOnTipItemClickListener(new PrivateDialog.OnTipItemClickListener() {
                    @Override
                    public void cancleClick() {
                        finishAffinity();
                    }

                    @Override
                    public void sureClick() {
                        //进入app
                    }

                    @Override
                    public void userClick() {
                        //跳转至用户服务协议
                        startActivity((new Intent(MainActivity.this, ProtocolActivity.class)));
                    }

                    @Override
                    public void termsClick() {

                        //跳转至隐私政策
                        startActivity((new Intent(MainActivity.this, TermActivity.class)));
                    }
                }).create(this);
    }
}